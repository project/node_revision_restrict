<?php

/**
 * @file
 * Calls results to administration's pages for the module.
 */

/**
 * Implements hook_form().
 */
function node_revision_restrict_form($form, &$form_state) {
  $content_types = node_type_get_types();
  $form['revision_restrict_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node revision restrict configuration'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  foreach ($content_types as $content_type_name) {
    $restrict_content_type_default_value = variable_get('restrict_node_revision_number_for_' . $content_type_name->type);
    $form['revision_restrict_group']['node_revision_restrict_content_type_' . $content_type_name->type] = array(
      '#type' => 'checkbox',
      '#title' => t('Content Type : <b>:content_type_title </b>', array(':content_type_title' => $content_type_name->name)),
      '#default_value' => isset($restrict_content_type_default_value) ? 1 : 0,
    );
    $form['revision_restrict_group']['node_revision_restrict_number_for_content_type_' . $content_type_name->type] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#description' => t('Enter number to restrict revisions or leave blank for no restrictions.'),
      '#title' => t('Revision limit for :content_type_title ?', array(':content_type_title' => $content_type_name->name)),
      '#default_value' => isset($restrict_content_type_default_value) ? $restrict_content_type_default_value : '',
      '#maxlength' => 128,
    );
  }
  $form['revision_restrict_group']['actions'] = array('#type' => 'actions');
  $form['revision_restrict_group']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Implements hook_form_validation().
 */
function node_revision_restrict_form_validate(&$form, &$form_state) {
  $content_types = node_type_get_types();
  foreach ($content_types as $content_type_name) {
    if (isset($form_state['values']['node_revision_restrict_content_type_' . $content_type_name->type])) {
      if ($form_state['values']['node_revision_restrict_content_type_' . $content_type_name->type] == 1 && $form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type] == '') {
        $form['node_revision_restrict_number_for_content_type_' . $content_type_name->type]['#required'] = TRUE;
        form_set_error('node_revision_restrict_number_for_content_type_' . $content_type_name->type, t('Enter numeric value you want to keep restrict number of revision for :content_type_name !', array(':content_type_name' => $content_type_name->name)));
      }
      elseif ($form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type] != '' && !is_numeric($form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type])) {
        form_set_error('node_revision_restrict_number_for_content_type_' . $content_type_name->type, t('Enter numeric value for text field <b>how many revisions do you want to keep for :content_type_name ?</b>', array(':content_type_name' => $content_type_name->name)));
      }
      elseif ($form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type] != '' && $form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type] < 1) {
        form_set_error('node_revision_restrict_number_for_content_type_' . $content_type_name->type, t('Enter more than 0 for text field <b>how many revisions do you want to keep for :content_type_name ?</b>', array(':content_type_name' => $content_type_name->name)));
      }
    }
    if (isset($form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type]) && !empty($form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type])) {
      if ($form_state['values']['node_revision_restrict_content_type_' . $content_type_name->type] != 1) {
        $form['node_revision_restrict_content_type_' . $content_type_name->type]['#required'] = TRUE;
        form_set_error('node_revision_restrict_content_type_' . $content_type_name->type, t('Checked :content_type_name checkbox!', array(':content_type_name' => $content_type_name->name)));
      }
    }
  }
}

/**
 * Implements hook_form_submit().
 */
function node_revision_restrict_form_submit($form, &$form_state) {
  $selected_content_types_and_value = array();
  $not_selected_content_types = array();
  $content_types = node_type_get_types();
  foreach ($content_types as $content_type_name) {
    if ($form_state['values']['node_revision_restrict_content_type_' . $content_type_name->type] == 1) {
      $selected_content_types_and_value[$content_type_name->type] = $form_state['values']['node_revision_restrict_number_for_content_type_' . $content_type_name->type];
    }
    else {
      $not_selected_content_types[$content_type_name->type] = $content_type_name->type;
    }
  }
  foreach ($selected_content_types_and_value as $content_type => $restrict_number) {
    variable_set('restrict_node_revision_number_for_' . $content_type, $restrict_number);
  }
  if (!empty($not_selected_content_types)) {
    foreach ($not_selected_content_types as $content_type) {
      $previous_set_variable = variable_get('restrict_node_revision_number_for_' . $content_type);
      if ($previous_set_variable) {
        variable_del('restrict_node_revision_number_for_' . $content_type);
      }
    }
  }
  drupal_set_message(t('The restrict node revision settings have been updated.'));
}
